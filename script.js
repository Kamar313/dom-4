let bodyElement = document.querySelector("body");

// Main Heading
let heading = document.createElement("header");
heading.style.width = "100%";
heading.style.height = "40px";
heading.style.backgroundColor = "red";
heading.style.textAlign = "center";
let headingtext = document.createElement("h1");
headingtext.innerText = "Flip Box";
heading.append(headingtext);
bodyElement.append(heading);

// Main Div
let mainDiv = document.createElement("div");
bodyElement.append(mainDiv);
mainDiv.style.display = "flex";
mainDiv.style.justifyContent = "space-between";

// div first
let divFirst = document.createElement("div");
divFirst.classList.add("parentOne");
divFirst.style.width = "45%";
divFirst.style.height = "600px";
mainDiv.append(divFirst);
divFirst.style.backgroundColor = "green";
let para = document.createElement("p");
para.innerText = "Without Event Deligation";

divFirst.append(para);

// inside parent Div
let divHolder = document.createElement("div");
divFirst.append(divHolder);
divHolder.style.display = "flex";
divHolder.style.flexWrap = "wrap";

divFirst.style.display = "flex";
divFirst.style.flexDirection = "column";

for (let index = 0; index < 12; index++) {
  let childWithoutDeligation = document.createElement("div");
  divHolder.append(childWithoutDeligation);
  childWithoutDeligation.style.width = "20%";
  childWithoutDeligation.style.height = "130px";
  childWithoutDeligation.style.backgroundColor = "red";
  childWithoutDeligation.style.border = "1px solid black";
  childWithoutDeligation.classList.add(`div`);
}
let allDiv = document.querySelectorAll(".div");

for (let i = 0; i < allDiv.length; i++) {
  allDiv[i].addEventListener("click", () => {
    let createH4 = document.createElement("h4");
    createH4.innerText = i;
    createH4.style.fontSize = "40px";
    allDiv[i].append(createH4);
    // allDiv[i].textContent = i;
    setTimeout(() => {
      allDiv[i].textContent = "";
    }, 5000);
  });
}

// Second Div

let divSecond = document.createElement("div");
divSecond.classList.add("parentTwo");
divSecond.style.width = "45%";
divSecond.style.height = "600px";
let para2 = document.createElement("p");
para2.innerText = "With Event Deligation";
divSecond.append(para2);
mainDiv.append(divSecond);
divSecond.style.backgroundColor = "green";
let divHolder2 = document.createElement("div");
divHolder2.style.display = "flex";
divHolder2.style.flexWrap = "wrap";

divSecond.append(divHolder2);
//
for (let i = 0; i < 12; i++) {
  let childDeligation = document.createElement("div");
  divHolder2.append(childDeligation);
  childDeligation.style.width = "20%";
  childDeligation.style.height = "130px";
  childDeligation.style.backgroundColor = "red";
  childDeligation.style.border = "1px solid black";
  childDeligation.classList.add(i);
}

divHolder2.addEventListener("click", (e) => {
  if (e.target.className) {
    e.target.innerText = e.target.className;
    e.target.style.fontSize = "40px";
    setTimeout(() => {
      e.target.innerText = "";
    }, 5000);
  }
});
